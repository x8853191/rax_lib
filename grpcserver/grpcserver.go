package grpcserver

import (
	"context"
	"net"
	"os"
	"os/signal"
	"syscall"

	"../../common/debug"
	healthcheck "./grpc.health.v1"
	"google.golang.org/grpc"
)

type server struct {
	srv *grpc.Server
}

func New(srv *grpc.Server, port string) {
	s := &server{}
	lis, err := net.Listen("tcp", port)
	if err != nil {
		panic(err.Error())
	}
	s.srv = srv
	healthcheck.RegisterHealthServer(srv, NewHealthChecker())
	go func() {
		if err := srv.Serve(lis); err != nil {
			debug.Log(err.Error())
		}
	}()
	go func() {
		s.signal()
	}()
}

func (this *server) signal() {
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	debug.ErrorLog("Server Shutdown")
	this.srv.GracefulStop()
}

type HealthChecker struct {
	healthcheck.UnimplementedHealthServer
}

func NewHealthChecker() *HealthChecker {
	return &HealthChecker{}
}

func (s *HealthChecker) Check(ctx context.Context, in *healthcheck.HealthCheckRequest) (*healthcheck.HealthCheckResponse, error) {
	return &healthcheck.HealthCheckResponse{
		Status: healthcheck.HealthCheckResponse_SERVING,
	}, nil
}
