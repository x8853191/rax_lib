package httpserver

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	debug "debugs"

	"github.com/gorilla/mux"
)

type server struct {
	srv *http.Server
}

func New(port string) *mux.Router {
	s := &server{}
	router := mux.NewRouter()
	router.HandleFunc("/health", s.health).Methods("GET")
	srv := &http.Server{
		Addr:    port,
		Handler: router,
	}
	s.srv = srv
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			debug.ErrorLog(err.Error())
		}
	}()
	go func() {
		s.signal()
	}()
	return router
}

func (this *server) signal() {
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	debug.ErrorLog("Server Shutdown")
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	if err := this.srv.Shutdown(ctx); err != nil {
		debug.ErrorLog(err.Error())
	}
}

func (this *server) health(res http.ResponseWriter, req *http.Request) {
	res.WriteHeader(http.StatusOK)
}
